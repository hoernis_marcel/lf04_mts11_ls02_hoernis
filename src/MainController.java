public class MainController {
    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5);

        klingonen.beladeRaumschiff(new Ladung("Ferengi Schneckensaft", 200));
        klingonen.beladeRaumschiff(new Ladung("Bat'leth Klingonen Schwert", 200));

        romulaner.beladeRaumschiff(new Ladung("Borg-Schrott", 5));
        romulaner.beladeRaumschiff(new Ladung("Rote Materie", 2));
        romulaner.beladeRaumschiff(new Ladung("Plasma-Waffe", 50));

        vulkanier.beladeRaumschiff(new Ladung("Forschungssonde", 35));
        vulkanier.beladeRaumschiff(new Ladung("Photonentorpedo", 3));

        klingonen.schussPhoton(romulaner);
        romulaner.schussPhaser(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandAusgeben();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.sendeReperaturauftrag(true, true, true, vulkanier.getReperaturandroiden());
        vulkanier.ladeInTorpedorohre(10);
        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.schussPhoton(romulaner);
        klingonen.schussPhoton(romulaner);
        klingonen.zustandAusgeben();
        klingonen.ladungsverzeichnisAusgeben();
        romulaner.zustandAusgeben();
        romulaner.ladungsverzeichnisAusgeben();
        vulkanier.zustandAusgeben();
        vulkanier.ladungsverzeichnisAusgeben();
        System.out.println(Raumschiff.gebeLogbuchZurueck());
    }
}
