public class Ladung {
    private String name;
    private int anzahl;

    Ladung(){}

    Ladung(String name, int anzahl)
    {
        this.name = name;
        this.anzahl = anzahl;
    }

    @Override

    public String toString(){
        return (this.name + "; " + this.anzahl);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
}
