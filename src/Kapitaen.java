public class Kapitaen {
    private String name;
    private int imAmtSeit;

    Kapitaen(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImAmtSeit() {
        return imAmtSeit;
    }

    public void setImAmtSeit(int imAmtSeit) {
        this.imAmtSeit = imAmtSeit;
    }
}
