import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class Raumschiff {
    private String name;
    private double energieversorgung;
    private double schutzschilde;
    private double lebenserhaltungssysteme;
    private double huelle;
    private int photonentorpedos;
    private int reperaturandroiden;
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();
    private static ArrayList<String> broadcastKommunikator = new ArrayList<>();
    private Kapitaen kapitaen;

    Raumschiff(){}

    Raumschiff(String name, double energieversorgung, double schutzschilde, double lebenserhaltungssysteme,
               double huelle, int photonentorpedos, int reperaturandroiden)
    {
        this.setName(name);
        this.setEnergieversorgung(energieversorgung);
        this.setSchutzschilde(schutzschilde);
        this.setLebenserhaltungssysteme(lebenserhaltungssysteme);
        this.setHuelle(huelle);
        this.setPhotonentorpedos(photonentorpedos);
        this.setReperaturandroiden(reperaturandroiden);
    }

    /**Fügt dem Ladungsverzeichnis des Raumschiffes eine neue Ladung hinzu
     * @param zuBeladendeLadung - Parameter enthält die neue hinzuzufügende Ladung*/
    public void beladeRaumschiff(Ladung zuBeladendeLadung){
        this.ladungsverzeichnis.add(zuBeladendeLadung);
    }

    /**Gibt den Zustand des Schiffes mit entsprechender Beschreibung aus*/
    public void zustandAusgeben(){
        System.out.println("Name: " + this.name);
        System.out.println("Energieversorgung: " + this.energieversorgung + "%");
        System.out.println("Schutzschilde: " + this.schutzschilde + "%");
        System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssysteme + "%");
        System.out.println("Huelle: " + this.huelle + "%");
        System.out.println("Photonentorpedos: " + this.photonentorpedos);
        System.out.println("Reperaturandroiden: " + this.reperaturandroiden);
    }

    /**Gibt die Ladung des Schiffes aus mit Namenbeschreibung und der jeweiligen Anzahl*/
    public void ladungsverzeichnisAusgeben(){
        for (Ladung ladung : this.ladungsverzeichnis) {
            System.out.println(ladung.toString());
        }
    }

    /**Funktion lädt aus Ladung Photonentorpedos zum Attribut photonentorpedos
     * @param anzahlTorpedos - die Anzahl an Photonentorpedos, die benötigt wird*/
    public void ladeInTorpedorohre(int anzahlTorpedos){
        boolean torpedosGefunden = false;
        int i;
        for(i = 0; i < this.ladungsverzeichnis.size(); i++){
            if(Objects.equals(this.ladungsverzeichnis.get(i).getName(), "Photonentorpedo")){
                torpedosGefunden = true;
                break;
            }
        }

        if(!torpedosGefunden){
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtAnAlle("-=*Click*=-");
        }
        else{
            if(anzahlTorpedos > ladungsverzeichnis.get(i).getAnzahl()){
                anzahlTorpedos = ladungsverzeichnis.get(i).getAnzahl();
            }
            ladungsverzeichnis.get(i).setAnzahl(this.ladungsverzeichnis.get(i).getAnzahl()-anzahlTorpedos);
            this.photonentorpedos += anzahlTorpedos;
            System.out.println(anzahlTorpedos + " Photonentorpedo(s) eingesetzt");
        }
    }

    /**Es wird überprüft, ob es Ladungen im ladungsverzeichnis gibt, die eine Anzahl von 0 haben und somit entfernt
     * werden können*/
    public void ladungsverzeichnisAufraeumen(){
        for(int i = 0; i < this.ladungsverzeichnis.size(); i++){
            if(this.ladungsverzeichnis.get(i).getAnzahl() == 0){
                this.ladungsverzeichnis.remove(i);
                i--;
            }
        }
    }

    /**Funktion ermittelt zuerst wie viele Androiden in den Einsatz geschickt werden können
     * Anschließend wird die Heilung ermittelt und den jeweiligen Schiffsstrukturen hinzugefügt
     * @param repariereSchutzschilde - gibt an, ob das Schutzschild repariert wird oder nicht
     * @param repariereEnergieversorgung - gibt an, ob die Energieversorgung repariert wird oder nicht
     * @param repariereSchiffshuelle - gibt an, ob die Hülle des Schiffs repariert wird oder nicht
     * @param anzahlAndroidenImEinsatz - gibt an wie viele Androiden in den Einsatz geschickt werden sollen*/
    public void sendeReperaturauftrag(boolean repariereSchutzschilde, boolean repariereEnergieversorgung, boolean repariereSchiffshuelle,
                                      int anzahlAndroidenImEinsatz){
        Random zufall = new Random();
        int zufallsZahl = zufall.nextInt(100);
        int anzahlAufTrueGesetzterSchiffsstrukturen = 0;
        if(anzahlAndroidenImEinsatz > this.reperaturandroiden){
            anzahlAndroidenImEinsatz = this.reperaturandroiden;
        }
        if(repariereEnergieversorgung){
            anzahlAufTrueGesetzterSchiffsstrukturen += 1;
        }
        if(repariereSchiffshuelle){
            anzahlAufTrueGesetzterSchiffsstrukturen += 1;
        }
        if(repariereSchutzschilde){
            anzahlAufTrueGesetzterSchiffsstrukturen += 1;
        }
        double ermittleHeilErgebnis = (double)(zufallsZahl * anzahlAndroidenImEinsatz) / anzahlAufTrueGesetzterSchiffsstrukturen;
        if(repariereEnergieversorgung){
            this.energieversorgung += ermittleHeilErgebnis;
        }
        if(repariereSchiffshuelle){
            this.huelle += ermittleHeilErgebnis;
        }
        if(repariereSchutzschilde){
            this.schutzschilde += ermittleHeilErgebnis;
        }
    }

    /**Funktion, die Photonentorpedos abschießt
     * @param zielschiff - Das Schiff, welches getroffen werden soll, wird hier übergeben*/
    public void schussPhoton(Raumschiff zielschiff){
        if(this.photonentorpedos < 1){
            nachrichtAnAlle("-=*Click*=-");
        }
        else {
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            zielschiff.raumschiffGetroffen();
            this.photonentorpedos--;
        }
    }

    /**Funktion, die die Phaserkanone abschießt
     * @param zielschiff - Das Schiff, welches getroffen werden soll, wird hier übergeben*/
    public void schussPhaser(Raumschiff zielschiff){
        if(this.energieversorgung < 50.0){
            nachrichtAnAlle("-=*Click*=-");
        }
        else{
            this.energieversorgung -= 50.0;
            nachrichtAnAlle("Phaserkanone abgeschossen");
            zielschiff.raumschiffGetroffen();
        }
    }

    /**Funktion ermittelt den Schaden, den das Raumschiff erfährt*/
    private void raumschiffGetroffen(){
        System.out.println(this.name + " wurde getroffen!");
        this.schutzschilde -= 50.0;

        if(this.schutzschilde <= 0.0){
            this.schutzschilde = 0.0;
            this.huelle -= 50.0;
            this.energieversorgung -= 50.0;

            if(this.huelle <= 0.0){
                this.huelle = 0.0;
                this.energieversorgung = 0.0;
                this.lebenserhaltungssysteme = 0.0;
                nachrichtAnAlle("Lebenserhaltungssysteme vernichtet");
            }
        }
    }

    /**Funktion die eine Nachricht dem Broadcast-Kommunikator hinzufügt
     * @param nachricht - übergibt eine Nachricht*/
    public void nachrichtAnAlle(String nachricht){
        broadcastKommunikator.add(nachricht);
    }

    /**Gibt den Broadcast-Kommunikator zurück
     * @return gibt das Logbuch/den Broadcast-Kommunikator zurück*/
    public static ArrayList<String> gebeLogbuchZurueck(){
        return broadcastKommunikator;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public double getEnergieversorgung() {
        return energieversorgung;
    }

    public void setEnergieversorgung(double energieversorgung) {
        this.energieversorgung = energieversorgung;
    }

    public double getSchutzschilde() {
        return schutzschilde;
    }

    public void setSchutzschilde(double schutzschilde) {
        this.schutzschilde = schutzschilde;
    }

    public double getLebenserhaltungssysteme() {
        return lebenserhaltungssysteme;
    }

    public void setLebenserhaltungssysteme(double lebenserhaltungssysteme) {
        this.lebenserhaltungssysteme = lebenserhaltungssysteme;
    }

    public double getHuelle() {
        return huelle;
    }

    public void setHuelle(double huelle) {
        this.huelle = huelle;
    }

    public int getPhotonentorpedos() {
        return photonentorpedos;
    }

    public void setPhotonentorpedos(int photonentorpedos) {
        this.photonentorpedos = photonentorpedos;
    }

    public int getReperaturandroiden() {
        return reperaturandroiden;
    }

    public void setReperaturandroiden(int reperaturandroiden) {
        this.reperaturandroiden = reperaturandroiden;
    }

    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }

    public ArrayList<String> getBroadcastKommunikator() {
        return broadcastKommunikator;
    }

    public void setBroadcastKommunikator(ArrayList<String> broadcastkommunikator) {
        broadcastKommunikator = broadcastkommunikator;
    }

    public Kapitaen getKapitaen() {
        return kapitaen;
    }

    public void setKapitaen(Kapitaen kapitaen) {
        this.kapitaen = kapitaen;
    }
}
